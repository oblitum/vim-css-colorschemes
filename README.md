#VIM colorschemes converted to CSS

All VIM colorschemes from [flazz repository](https://github.com/flazz/vim-colorschemes)
that were able to be automatically converted with [vim2pygments](https://github.com/honza/vim2pygments).

They have been automatically separated in light and dark backgrounds too.
